/** @format */

import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import passwd from '../../images/password.png';
import mailpic from '../../images/MailSent.png';

import { PropagateLoader } from 'react-spinners';
import { Link, useNavigate } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import axios from 'axios';
import './MailSent.scss';

const ChangePassword = () => {
  let { token } = useParams();
  let { userId } = useParams();
  const navigate = useNavigate();

  const [email, setEmail] = useState('');

  const [reqloading, setReqLoading] = useState(true);

  const [IsValid, SetIsValid] = useState(false);
  let errors = [];

  useEffect(() => {
    axios
      .get(`http://127.0.0.1:5000/password-reset/${userId}/${token}`)
      .then((res) => {
        if (res.data.success) {
          console.log(res.data.success);
          setReqLoading(false);
          SetIsValid(true);
        }
      })
      .catch((err) => {
        console.log(err);
        setReqLoading(false);
        SetIsValid(false);
      });
  }, []);

  function formValidation(data) {
    let reg =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    errors = [];
    for (let key in data) {
      if (data[key] === '') {
        errors.push(`${key} is empty!😊`);
      }
      if (key === 'email' && data[key] !== '' && !reg.test(data[key])) {
        errors.push(`${key} is not valid!😊`);
      }
    }
  }

  function submitHandler(e) {
    e.preventDefault();
    let data = {
      email,
    };
    formValidation(data);
    if (errors.length !== 0) {
      errors.forEach((error, i) => {
        toast.error(error, {
          position: 'top-right',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      });
      return;
    }
    setReqLoading(true);
    axios
      .post(`http://127.0.0.1:5000/user/reset-password`, data)
      .then((response) => {
        setReqLoading(false);
        // SetMailSent(true);
        // navigate('/login');
      })
      .catch((error) => {
        setReqLoading(false);
        console.clear();
        toast.error('email not found , please try another one😊', {
          position: 'top-right',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      });
  }

  return (
    <div>
      <div className='mail-sent'>
        <div className='mail-form'>
          {reqloading ? (
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                position: 'relative',
                top: '30px',
              }}>
              <PropagateLoader
                color={'#F5A623'}
                css={{ top: '-50px', left: '-20px' }}
                size={60}
                loading={true}
              />
            </div>
          ) : IsValid ? (
            <>
              <h2>Changement de mot de passe</h2>

              <h4>Saisir votre Nouvelle Mot de passe</h4>
              <div className='mail-form-content'>
                <span> Mot de passe</span>
                <input
                  placeholder='passowrd'
                  type='password'
                  name='email'
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
                <span>Confirmer le mot de passe</span>

                <input
                  placeholder='passowrd'
                  type='password'
                  name='email'
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
                <button className='submit' onClick={(e) => submitHandler(e)}>
                  Changer
                </button>
              </div>
            </>
          ) : (
            <div>
              <h1>NotValid</h1>
            </div>
          )}
        </div>
      </div>
      <ToastContainer
        position='top-right'
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme='colored'
      />
    </div>
  );
};

export default ChangePassword;
