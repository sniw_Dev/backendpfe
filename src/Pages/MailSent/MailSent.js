/** @format */

import React, { useState, useEffect } from 'react';
import mailpic from '../../images/MailSent.png';
import passwd from '../../images/password.png';
import { PropagateLoader } from 'react-spinners';
import { Link, useNavigate } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';

import axios from 'axios';

import './MailSent.scss';

const MailSent = () => {
  const navigate = useNavigate();

  const [email, setEmail] = useState('');

  const [reqloading, setReqLoading] = useState(false);

  const [MailSent, SetMailSent] = useState(false);
  let errors = [];

  function formValidation(data) {
    let reg =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    errors = [];
    for (let key in data) {
      if (data[key] === '') {
        errors.push(`${key} is empty!😊`);
      }
      if (key === 'email' && data[key] !== '' && !reg.test(data[key])) {
        errors.push(`${key} is not valid!😊`);
      }
    }
  }

  function submitHandler(e) {
    e.preventDefault();
    let data = {
      email,
    };
    formValidation(data);
    if (errors.length !== 0) {
      errors.forEach((error, i) => {
        toast.error(error, {
          position: 'top-right',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      });
      return;
    }
    setReqLoading(true);
    axios
      .post(`http://127.0.0.1:5000/user/reset-password`, data)
      .then((response) => {
        setReqLoading(false);
        SetMailSent(true);
        // navigate('/login');
      })
      .catch((error) => {
        setReqLoading(false);
        console.clear();
        toast.error('email not found , please try another one😊', {
          position: 'top-right',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      });
  }

  return (
    <div>
      {MailSent ? (
        <div className='mail-sent'>
          <div className='mail-sent-content'>
            <div className='mail-sent-content-warp'>
              <div className='mail-sent-content-img'>
                <img src={mailpic} alt='mail' />
              </div>
              <h2>l'e-mail de récupération a été envoyé avec succès</h2>
            </div>
          </div>
        </div>
      ) : null}
      <div className='mail-sent'>
        <div className='mail-form'>
          {reqloading ? (
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                position: 'relative',
                top: '30px',
              }}>
              <PropagateLoader
                color={'#F5A623'}
                css={{ top: '-50px', left: '-20px' }}
                size={60}
                loading={true}
              />
            </div>
          ) : (
            <>
              <img src={passwd} alt='mail' />

              <h2>Récupération de mot de passe</h2>

              <h4>Saisir votre addresse email</h4>
              <div className='mail-form-content'>
                <input
                  placeholder='Email'
                  type='email'
                  name='email'
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
                <button className='submit' onClick={(e) => submitHandler(e)}>
                  Envoyer
                </button>
              </div>
            </>
          )}
        </div>
      </div>
      <ToastContainer
        position='top-right'
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme='colored'
      />
    </div>
  );
};

export default MailSent;
