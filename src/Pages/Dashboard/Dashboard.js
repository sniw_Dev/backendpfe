/** @format */

import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import DashAside from '../../Components/Dashboard/DashAside';
import './dashboard.scss';
import { Outlet, useNavigate } from 'react-router-dom';
import Loading from '../../Components/Loading/Loading';
import { io } from 'socket.io-client';
import { toast } from 'react-toastify';

const Dashboard = () => {
  //eslint-disable-next-line
  const socket = io('http://127.0.0.1:5000');

  const navigate = useNavigate();
  const user = useSelector((state) => state.user.user);
  const loading = useSelector((state) => state.user.loading);
  // const isAuth = useSelector(state => state.user.isAuth)
  let audio = new Audio('/order.mp3');

  useEffect(() => {
    socket.on('order:new', (args) => {
      console.log('new order');
      audio.play();
      toast.success('New order placed successfully😉');
    });
  }, []);
  useEffect(() => {
    if (!user && !loading) {
      navigate('/login');
    }
  }, [navigate, user, loading]);

  return (
    <>
      {!user ? (
        <Loading loading={loading} />
      ) : (
        <div className='dash_container'>
          <div className='dash_aside'>
            <DashAside user={user} />
          </div>
          <div className='dash_content'>
            <Outlet />
          </div>
        </div>
      )}
    </>
  );
};

export default Dashboard;
