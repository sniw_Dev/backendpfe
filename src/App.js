/** @format */

import React, { Fragment, useEffect } from 'react';
import { Route, Routes } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { getUser } from './Store/UserSlice';
import { getCart } from './Store/cartSlice';
import Login from './Pages/Login/Login';
import SignUp from './Pages/Signup/SignUp';
import PageNotFound from './Pages/PageNotFound/PageNotFound';
import Dashboard from './Pages/Dashboard/Dashboard';
import DashboardContent from './Components/Dashboard/DashboardContent';
import Users from './Components/Dashboard/Users';
import Admins from './Components/Dashboard/Admins';
import Products from './Components/Dashboard/Products/ProductList/Products';
import Commande from './Components/Dashboard/Commande/Commande';
import DashboardOrders from './Components/Dashboard/Orders';
import Tables from './Components/Dashboard/Tables/Tables';
import MailSent from './Pages/MailSent/MailSent';
import ChangePassword from './Pages/MailSent/ChangePassword';
import { Table } from '@mui/material';

const App = () => {
  const dispatch = useDispatch();
  // eslint-disable-next-line
  function setUserStore() {
    let token = localStorage.token;
    let cart = localStorage.cart;
    if (token) {
      dispatch(getUser());
    }
    if (cart) {
      dispatch(getCart());
    }
  }

  useEffect(() => {
    setUserStore();
  }, [setUserStore]);

  return (
    <Fragment>
      <Routes>
        <Route path='/'>
          <Route path='signup' element={<SignUp />} />
          <Route path='login' element={<Login />} />
          <Route path='forgot-password' element={<MailSent />} />
          <Route
            path='password-reset/:userId/:token'
            element={<ChangePassword />}
          />
        </Route>
        <Route path='/dashboard' element={<Dashboard />}>
          <Route index element={<DashboardContent />} />
          <Route path='/dashboard/users' element={<Users />} />
          <Route path='/dashboard/admins' element={<Admins />} />
          <Route path='/dashboard/products' element={<Products />} />
          <Route path='/dashboard/orders' element={<DashboardOrders />} />
          <Route path='/dashboard/Tables' element={<Tables />} />
          <Route path='/dashboard/commande' element={<Commande />} />
        </Route>
        <Route path='*' element={<PageNotFound />} />
      </Routes>
    </Fragment>
  );
};

export default App;
