/** @format */

import React, { useReducer, useState, useEffect } from 'react';
import CheckOutProduct from '../Helpers/CheckOutProduct';
import './checkoutprodside.scss';
import { useSelector, useDispatch } from 'react-redux';
import { cartActions } from '../../Store/cartSlice';
import axios from 'axios';
import ReqLoading from '../Loading/ReqLoading';
import { toast } from 'react-toastify';

let formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
});

const CheckOutProdSide = (props) => {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user.user);
  const cart = useSelector((state) => state.cart.cart);
  const [loading, setLoading] = useState(false);
  const [currentuser, setcurrentuser] = useState();
  const [currentcart, setcurrentcart] = useState();
  useEffect(() => {
    props.socket.emit('connection', 'test');

    // setcurrentuser(JSON.parse(localStorage.getItem('table')));
    // console.log(JSON.parse(localStorage.getItem('table')));
  }, []);
  useEffect(() => {
    console.log(currentuser, 'currentuser');
  }, [currentuser]);

  function addToCart(data) {
    let postData = {
      userId: JSON.parse(localStorage.getItem('table'))._id,
      qnt: cart.qnt + 1,
      totalPrice: cart.totalPrice + data.price,
      products: null,
    };
    console.log(postData, 'postData');
    let products = cart.products.map((prod) => {
      if (prod.prodId === data.prodId) {
        return Object.assign({}, prod, {
          qnt: prod.qnt + 1,
          totalPrice: prod.totalPrice + data.price,
        });
      }
      return prod;
    });
    postData.products = products;
    console.log(postData);
    dispatch(cartActions.addToCart(postData));
    axios
      .post(`http://127.0.0.1:5000/cart/add`, postData, {
        headers: {
          Authorization: `Bearer ${localStorage.token}`,
        },
      })
      .then((res) => {})
      .catch((error) => {
        console.log(error);
      });
  }

  function removeOneItem(data) {
    let postData = {
      qnt: cart.qnt - 1,
      totalPrice: cart.totalPrice - data.price,
      products: null,
    };
    let isExist = cart.products.find((prod) => prod.prodId === data.prodId);

    if (isExist.qnt === 1) {
      postData.products = cart.products.filter(
        (prod) => prod.prodId !== data.prodId
      );
    } else {
      let products = cart.products.map((prod) => {
        if (prod.prodId === data.prodId) {
          return Object.assign({}, prod, {
            qnt: prod.qnt - 1,
            totalPrice: prod.totalPrice - data.price,
          });
        }
        return prod;
      });
      postData.products = products;
    }
    dispatch(cartActions.removeOneItem(data));

    axios
      .post(`http://127.0.0.1:5000/cart/add`, postData, {
        headers: {
          Authorization: `Bearer ${localStorage.token}`,
        },
      })
      .then((res) => {})
      .catch((error) => {
        console.log(error);
      });
  }

  function removeItem(data) {
    let isExist = cart.products.find((prod) => prod.prodId === data.prodId);
    let postData = {
      qnt: cart.qnt - isExist.qnt,
      totalPrice: cart.totalPrice - isExist.totalPrice,
      products: null,
    };
    postData.products = cart.products.filter(
      (prod) => prod.prodId !== data.prodId
    );
    dispatch(cartActions.removeItem(data));
    axios
      .post(`http://127.0.0.1:5000/cart/add`, postData, {
        headers: {
          Authorization: `Bearer ${localStorage.token}`,
        },
      })
      .then((res) => {})
      .catch((error) => {
        console.log(error);
      });
  }

  function orderHandler() {
    setLoading(true);
    axios
      .post(`http://127.0.0.1:5000/order`, cart, {
        headers: {
          Authorization: `Bearer ${localStorage.token}`,
        },
      })
      .then((res) => {
        dispatch(cartActions.logout());
        props.socket.emit('order:create', res.data);
        toast.success('your order placed successfully😉');
        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  useEffect(() => {
    console.log(cart);
    if (currentuser) {
      if (cart) {
        // cart.userId = currentuser._id;
        setcurrentcart(cart);
      }
    }
  }, [cart]);

  useEffect(() => {
    console.log(currentcart);
  }, [currentcart]);
  return (
    <div className='product_side'>
      {/* start head */}
      <div className='product_side_head'>
        <div className='product_side_title'>
          <h3>Shopping Cart</h3>
          <p>you have {cart.qnt} items in your cart</p>
        </div>
        <div className='sort'>
          <button className='checkout_btn' onClick={orderHandler}>
            <span>{formatter.format(cart.totalPrice)}</span>
            <span>
              checkout <i className='fas fa-arrow-right'></i>
            </span>
          </button>
        </div>
      </div>
      {/* end head */}

      {/* start product container */}
      <div className='checkout_product_wrapper'>
        {cart.products.map((prod) => (
          <CheckOutProduct
            key={prod.prodId}
            removeOneItem={removeOneItem}
            removeItem={removeItem}
            addToCart={addToCart}
            prod={prod}
          />
        ))}
      </div>
      {/* end  product container */}
    </div>
  );
};

export default CheckOutProdSide;
