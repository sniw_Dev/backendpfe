/** @format */

import React, { useEffect, useState } from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import image from './a.jpg';
import { Link } from 'react-router-dom';

export default function MediaCard(props) {
  useEffect(() => {
    console.log('cat', props.cat);
  }, [props.cat]);
  return (
    <Card
      sx={{
        maxWidth: 345,
        height: '100%',
        marginBottom: '10px',
        borderRadius: '20px',
      }}>
      {/* <Link key={props.cat._id} to={`/products?search=${props.cat.name}`}>
        <CardMedia
          component='img'
          height='100'
          image={props.cat.icon}
          alt='green iguana'
        />
      </Link> */}

      <Typography
        gutterBottom
        component='div'
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        {props.cat.name}
      </Typography>
    </Card>
  );
}
