/** @format */

import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import ShoppingBagRoundedIcon from '@mui/icons-material/ShoppingBagRounded';
import axios from 'axios';
import { toast } from 'react-toastify';
import { useSelector, useDispatch } from 'react-redux';
import { cartActions } from '../../Store/cartSlice';

export default function ProductCard(props) {
  const dispatch = useDispatch();
  const isAuth = useSelector((state) => state.user.isAuth);
  const cart = useSelector((state) => state.cart.cart);

  let data = {
    prodId: props.id,
    title: props.title,
    company: props.company,
    image: props.img,
    price: parseInt(props.price),
  };

  function addCart(e) {
    e.preventDefault();
    if (!isAuth) return toast.warning('login or register please😊');

    let isValid = cart.products.find((prod) => prod.prodId === data.prodId);
    let postData = {
      qnt: cart.qnt + 1,
      totalPrice: cart.totalPrice + data.price,
      products: null,
    };
    if (!isValid) {
      postData.products = cart.products.concat({
        ...data,
        qnt: 1,
        totalPrice: data.price,
      });
    } else {
      let products = cart.products.map((prod) => {
        if (prod.prodId === data.prodId) {
          return Object.assign({}, prod, {
            qnt: prod.qnt + 1,
            totalPrice: prod.totalPrice + data.price,
          });
        }
        return prod;
      });
      postData.products = products;
    }
    dispatch(cartActions.addToCart(data));
    toast.success('product added to cart successfully😊');

    axios
      .post(`http://127.0.0.1:5000/cart/add`, postData, {
        headers: {
          Authorization: `Bearer ${localStorage.token}`,
        },
      })
      .then((res) => {})
      .catch((error) => {
        console.log(error);
      });
  }

  return (
    <Card
      sx={{
        maxWidth: 180,
        minWidth: 180,
        marginBottom: '20px',
        marginTop: '20px',
        borderRadius: '20px',
      }}>
      {/* <IconButton aria-label='settings'>
        <Chip
          label='Sucré & Salé	'
          sx={{
            background: '#f2c078',
            color: '#fff',
            position: 'relative',
            top: '5px',
            right: '0',
          }}
        />
      </IconButton> */}

      <CardMedia
        component='img'
        height='100'
        image={props.img}
        alt='green iguana'
        sx={{ borderRadius: '20px' }}
      />
      <CardContent>
        <Typography
          gutterBottom
          component='div'
          style={{
            fontSize: '12px',
            whiteSpace: 'nowrap',
            overflow: 'hidden',
          }}>
          {props.title}
        </Typography>
        {/* <Typography
          gutterBottom
          component='div'
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          {props.price} TND
          <FavoriteIcon sx={{ color: '#f2c078' }} />
        </Typography> */}
        <Button
          onClick={(e) => addCart(e)}
          variant='contained'
          style={{ width: '100%', backgroundColor: '#f2c078', color: '#fff' }}
          endIcon={<ShoppingBagRoundedIcon />}>
          {props.price} TND
        </Button>
      </CardContent>
    </Card>
  );
}
