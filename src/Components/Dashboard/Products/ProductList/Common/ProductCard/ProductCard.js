/** @format */

import React, { useEffect, useState } from 'react';
import './ProductCard.scss';
import Button from '@mui/material/Button';
import AutoFixHighIcon from '@mui/icons-material/AutoFixHigh';
const ProductCard = (props) => {
  useEffect(() => {
    console.log(props.prod);
  }, [props.prod]);

  useEffect(() => {
    console.log(props.cat);
  }, [props.cat]);

  return (
    <div className='CARD'>
      {props.type === 'product' ? (
        <>
          <div className='card-header'>
            <img src={props.prod.images[0]} alt='' />
          </div>

          <div className='card-content'>
            <div>
              {' '}
              <span>{props.prod.title}</span>
            </div>
            <div>
              <span>{props.prod.price}DT</span>
            </div>
          </div>

          <div className='card-footer'>
            <Button
              onClick={() => {
                props.selectedProduct(props.prod, 'Edit');
              }}
              style={{ color: '#fff' }}
              className='btn_edit'
              variant='contained'
              startIcon={<AutoFixHighIcon />}>
              {' '}
              Modifier
            </Button>
          </div>
        </>
      ) : (
        <>
          <div className='card-header'>
            <img src={props.cat.icon} alt='' />
          </div>

          <div className='card-content'>
            <div>
              {' '}
              <span>{props.cat.name}</span>
            </div>
          </div>

          <div className='card-footer'>
            <Button
              onClick={() => {
                props.selectedCategorie(props.cat, 'Edit');
              }}
              style={{ color: '#fff' }}
              className='btn_edit'
              variant='contained'
              startIcon={<AutoFixHighIcon />}>
              {' '}
              Modifier
            </Button>
          </div>
        </>
      )}
    </div>
  );
};

export default ProductCard;
