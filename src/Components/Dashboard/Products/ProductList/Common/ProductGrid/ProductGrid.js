/** @format */

import React, { useState, useEffect } from 'react';
import './ProductGrid.scss';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import RepeatIcon from '@mui/icons-material/Repeat';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import ProductCard from '../ProductCard/ProductCard';
import axios from 'axios';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { useSelector } from 'react-redux';
import HashLoader from 'react-spinners/HashLoader';

const theme = createTheme({
  palette: {
    primary: {
      // Purple and green play nicely together.
      main: '#EA7C69',
    },
    secondary: {
      // This is green.A700 as hex.
      main: '#1f1d2b',
    },
  },
});

const ProductGrid = (props) => {
  const [value, setValue] = React.useState('one');
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [ActiveCategory, SetActiveCategory] = useState(0);
  const [CatList, SetCatList] = useState([]);
  const [SelectedProduct, SetSelectedProduct] = useState();
  const [selectedCategories, setSelectedCategories] = useState();
  const [ActionModen, SetActionMode] = useState(false);
  const [GridMode, SetGridMode] = useState(true);
  const user = useSelector((state) => state.user.user);

  useEffect(() => {
    console.log('Deleteici');
    SetActiveCategory();
    SetActiveCategory('autres');
    fetchCategories();
  }, [props.candleCatUpdate]);

  useEffect(() => {
    console.log(user, 'user');
  }, [user]);
  useEffect(() => {
    console.log(products, 'products');
  }, [products]);

  useEffect(() => {
    console.log(ActiveCategory, 'ActiveCategory');
    if (ActiveCategory === CatList.length + 1) {
      fetchCategoriesHandler();
    } else {
      if (CatList.length > 0) {
        fetchdata();
      }
    }
  }, [ActiveCategory]);

  useEffect(() => {
    fetchCategories();
  }, []);

  useEffect(() => {
    console.log(CatList, 'CatList');
    if (CatList.length !== 0) {
      fetchdata();
    }
  }, [CatList]);

  useEffect(() => {
    if (props.Updated === true) {
      fetchCategories();

      props.updatehandler();
    }
  }, [props.Updated]);

  const fetchCategories = () => {
    setLoading(true);
    axios
      .get(`http://127.0.0.1:5000/category`, {
        headers: {
          Authorization: `Bearer ${localStorage.token}`,
        },
      })
      .then((res) => {
        props.CategoriesHandler(res.data);
        SetCatList([...res.data]);
        if (ActiveCategory !== 0) {
          res.data.map((item, index) => {
            if (index === ActiveCategory) {
              SetActiveCategory(index);
            }
          });
        } else {
          SetActiveCategory(0);
        }

        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const fetchdata = () => {
    console.log(CatList);
    try {
      axios
        .get(
          `http://localhost:5000/api/v1/${user._id}/product/category/${CatList[ActiveCategory].name}`
        )
        .then((res) => {
          setProducts(res.data);
          setLoading(false);
        });
    } catch (error) {
      fetchCategoriesHandler();
    }
  };

  const fetchCategoriesHandler = () => {
    console.log(CatList);
    axios
      .get(`http://localhost:5000/api/v1/${user._id}/product/category/autres`)
      .then((res) => {
        setProducts(res.data);
        setLoading(false);
      });
  };

  const handleSelectedProd = (product, Mode) => {
    console.log(product, Mode);
    SetSelectedProduct(product);
    SetActionMode(Mode);
    props.EditHandler({ product, Mode });
  };

  const handleChange = (event, newValue) => {
    console.log(newValue, 'cat_id');

    SetActiveCategory(newValue);
  };

  const CategoriesHandler = () => {
    SetGridMode(!GridMode);
  };

  const handleSelectedCategorie = (cat, Mode) => {
    console.log(cat, 'cat');
    setSelectedCategories({ cat, Mode });
    props.HandleEditCategories({ cat, Mode });
  };

  return (
    <ThemeProvider theme={theme}>
      <div className='ProdGrid__Container'>
        {GridMode === true ? (
          <div className='ProdGrid__header_container'>
            <div className='ProdGrid__header'>
              <span>Products Management</span>
            </div>
            <div className='ProdGrid__header__2'>
              <Button
                variant='outlined'
                className='CategoriesButton'
                onClick={() => CategoriesHandler()}
                startIcon={<RepeatIcon />}>
                Manage Categories
              </Button>{' '}
            </div>
          </div>
        ) : (
          <div className='ProdGrid__header_container'>
            <div className='ProdGrid__header'>
              <span>Categorie Management</span>
            </div>
            <div className='ProdGrid__header__2'>
              <Button
                variant='outlined'
                className='CategoriesButton'
                onClick={() => CategoriesHandler()}
                startIcon={<RepeatIcon />}>
                Manage Products
              </Button>{' '}
            </div>
          </div>
        )}
        {GridMode === true ? (
          <div className='ProdGrid__body'>
            <Tabs
              style={{}}
              value={ActiveCategory}
              onChange={handleChange}
              textColor='primary'
              indicatorColor='primary'
              aria-label='secondary tabs example'>
              {CatList.length !== 0 &&
                CatList.map((cat, index) => (
                  <Tab
                    style={{ color: '#EA7C69' }}
                    value={index}
                    label={cat.name}
                    onChange={handleChange}
                  />
                ))}
              <Tab
                style={{ color: '#EA7C69' }}
                value={CatList.length + 1}
                label={'autres'}
                onChange={handleChange}
              />
            </Tabs>
            {loading === true ? (
              <HashLoader color={'#E1E8EB'} size={30} />
            ) : (
              <Grid container spacing={3} style={{ marginTop: '20px' }}>
                <Grid item xs={12} sm={4}>
                  <div className='card_main'>
                    <div className='card-body'>
                      <div>
                        <Button
                          style={{
                            fontSize: '70px',
                            height: '40px',
                            marginBottom: '30px',
                          }}
                          variant='text'
                          onClick={() => {
                            props.sidebar({ type: 'product' });
                          }}>
                          +
                        </Button>
                      </div>
                      <div>
                        <span>Ajouter un article</span>
                      </div>
                    </div>
                  </div>
                </Grid>
                {products.map((product) => (
                  <Grid item xs={12} sm={4}>
                    <ProductCard
                      selectedProduct={handleSelectedProd}
                      prod={product}
                      type={'product'}
                    />
                  </Grid>
                ))}
              </Grid>
            )}
          </div>
        ) : (
          <div className='ProdGrid__body'>
            <Grid container spacing={3} style={{ marginTop: '20px' }}>
              <Grid item xs={12} sm={4}>
                <div className='card_main'>
                  <div className='card-body'>
                    <div>
                      <Button
                        style={{ fontSize: '70px' }}
                        variant='text'
                        onClick={() => {
                          props.sidebarcat({ type: 'category' });
                        }}>
                        +
                      </Button>
                    </div>
                    <div>
                      <span>Ajouter une Categorie</span>
                    </div>
                  </div>
                </div>
              </Grid>
              {CatList.length > 0 &&
                CatList.map((cat, index) => (
                  <Grid item xs={12} sm={4}>
                    <ProductCard
                      selectedCategorie={handleSelectedCategorie}
                      cat={cat}
                      type={'category'}
                    />
                  </Grid>
                ))}
            </Grid>
          </div>
        )}
      </div>
    </ThemeProvider>
  );
};

export default ProductGrid;
