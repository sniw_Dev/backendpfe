/** @format */

import React, { useEffect, useState } from 'react';
import AddProduct from '../../../ProductFormAdd/AddProduct';
import Categories from '../../Categories/Categories';
import './ProductMenu.scss';
const ProductMenu = (props) => {
  useEffect(() => {
    console.log(props);
    console.log(props.Type, 'prod');
  }, [props.prod]);
  return (
    <div className='productMenuContainer'>
      <div
        style={{ display: 'flex', justifyContent: 'flex-end', width: '100%' }}>
        <button type='button' class='btn-close' onClick={props.handleCloseMenu}>
          <span class='icon-cross'></span>
          <span class='visually-hidden'>Close</span>
        </button>
      </div>
      {props.Type === 'product' ? (
        <AddProduct
          Categories={props.Categories}
          MenuHandler={props.MenuHandler}
          updatehandler={props.updatehandler}
        />
      ) : (
        <Categories
          CatDeletehandler={props.CatDeletehandler}
          updatehandler={props.updatehandler}
          MenuHandler={props.MenuHandler}
          CatHandler={props.CatHandler}
        />
      )}
    </div>
  );
};

export default ProductMenu;
