/** @format */

import React, { useEffect, useState, useRef } from 'react';
import { useSelector } from 'react-redux';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import { styled } from '@mui/material/styles';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import PhotoCamera from '@mui/icons-material/PhotoCamera';
import Swal from 'sweetalert2';
import HashLoader from 'react-spinners/HashLoader';

import './Categories.scss';
const Categories = (props) => {
  const [category, setCategory] = useState('');
  const [icon, setIcon] = useState(
    'https://www.kindpng.com/picc/m/45-451229_custom-upload-youtube-logo-hd-png-download.png'
  );
  const [data, setData] = useState([]);
  const [reqloading, setReqLoading] = useState(false);
  const [loading, setLoading] = useState(false);
  const [SubmitState, SetSubmitState] = useState('Ajouter');
  const [ActiveCat, SetActiveCat] = useState();
  const [caticon, setCaticon] = useState();
  const user = useSelector((state) => state.user.user);
  const formRef = useRef(null);

  const Input = styled('input')({
    display: 'none',
  });

  useEffect(() => {}, [caticon]);
  useEffect(() => {}, [icon, SubmitState]);

  useEffect(() => {
    if (props.CatHandler.Mode.includes('Edit')) {
      SetSubmitState('Modifier');
      setCategory(props.CatHandler.cat.name);
      SetActiveCat(props.CatHandler.cat);
      setIcon(props.CatHandler.cat.icon);
    } else {
      SetSubmitState('Ajouter');
      setCategory('');
      setIcon(
        'https://www.kindpng.com/picc/m/45-451229_custom-upload-youtube-logo-hd-png-download.png'
      );
      setCaticon('');
    }
  }, [props]);

  let errors = [];

  function formValidation(data) {
    errors = [];
    console.log('data', data);
    for (let key in data) {
      if (caticon === undefined && props.CatHandler.Mode.includes('add')) {
        errors.push(`photos is empty!`);
      }
      if (data[key] === '') {
        errors.push(`${key} is empty!😊`);
      }
    }
  }

  const submitHandler = (e) => {
    let formData = new FormData();

    e.preventDefault();
    let datac = {
      name: category,
    };

    formValidation(datac);

    if (errors.length !== 0) {
      errors.forEach((error, i) => {
        toast.error(error, {
          position: 'top-right',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      });
      return;
    }
    for (let key in datac) {
      formData.append(key, datac[key]);
    }
    if (caticon) {
      [...caticon].forEach((caticon) => {
        formData.append('photos', caticon);
      });
    }

    setReqLoading(true);

    if (SubmitState.includes('Ajouter')) {
      setLoading(true);
      axios
        .post(
          `http://127.0.0.1:5000/api/v1/${user._id}/categories/add/`,
          formData,
          {
            headers: {
              Authorization: `Bearer ${localStorage.token}`,
            },
          }
        )
        .then((res) => {
          setLoading(false);
          setData((data) => data.concat(res.data));
          toast.success('category add successfully😁', {
            position: 'top-right',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          setCategory('');
          setIcon(
            'https://www.kindpng.com/picc/m/45-451229_custom-upload-youtube-logo-hd-png-download.png'
          );
          props.updatehandler();
        })
        .catch((error) => {
          console.log(error);
        });
      //http://127.0.0.1:5000/api/v1/${user._id}/categories/update/${ActiveCat._id}
      //SetSubmitState;
    } else if (SubmitState.includes('Modifier')) {
      console.log('Modifier');
      setLoading(true);
      axios
        .post(
          `http://127.0.0.1:5000/api/v1/${user._id}/categories/update/${ActiveCat._id}`,
          formData,
          {
            headers: {
              Authorization: `Bearer ${localStorage.token}`,
            },
          }
        )
        .then((res) => {
          setLoading(false);
          console.log(data.findIndex((c) => c._id === res.data._id));
          let datatem = data;
          datatem[data.findIndex((c) => c._id === res.data._id)] = res.data;
          console.log(datatem, 'dataupdated');
          console.log(data, 'final data');
          setData(datatem);

          toast.success(
            `la categorie ${ActiveCat.name}  est Modifié avec succes`,
            {
              position: 'top-right',
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            }
          );
          props.updatehandler();

          setCategory('');
          setIcon('');
          SetSubmitState('Ajouter');
          setReqLoading(false);
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };
  // function deleteCategory(id) {
  //   setReqLoading(true);
  //   axios
  //     .delete(`http://127.0.0.1:5000/category/delete/${id}`, {
  //       headers: {
  //         Authorization: `Bearer ${localStorage.token}`,
  //       },
  //     })
  //     .then((res) => {
  //       setData((data) =>
  //         data.filter((category) => category._id !== res.data._id)
  //       );
  //       setReqLoading(false);
  //       toast.success(`${res.data.name} deleted successfully`, {
  //         position: 'top-right',
  //         autoClose: 5000,
  //         hideProgressBar: false,
  //         closeOnClick: true,
  //         pauseOnHover: true,
  //         draggable: true,
  //         progress: undefined,
  //       });
  //     })
  //     .catch((error) => {
  //       console.log(error);
  //     });
  // }

  // function HandleEditState(cat) {
  //   setCategory(cat.name);
  //   setIcon(cat.icon);
  //   SetSubmitState('Modifier');
  //   SetActiveCat(cat);
  // }

  const handleDeleteCategorie = () => {
    Swal.fire({
      title: 'Confirmer la suppression',
      text: 'voulez vous supprimer cette categorie  ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Annuler',

      confirmButtonText: 'Oui, supprimer!',
    }).then((result) => {
      if (result.value) {
        console.log('swal', result.value);
        ///api/v1/:userId/categories/delete/:id
        setLoading(true);
        axios
          .post(
            `http://127.0.0.1:5000/api/v1/${user._id}/categories/delete/${ActiveCat._id}`
          )
          .then((res) => {
            setLoading(false);
            props.CatDeletehandler();
            console.log('ici');
            setCategory('');
            setIcon('');
            SetSubmitState('Ajouter');
            setReqLoading(false);
            props.updatehandler();
            toast.success(`la Categorie est supprimé avec succès 😁`);

            // ResetForm();
          })
          .catch((error) => {});
      }
    });
  };

  useEffect((_) => {
    // setLoading(true);
    // axios
    //   .get(`http://127.0.0.1:5000/category`, {
    //     headers: {
    //       Authorization: `Bearer ${localStorage.token}`,
    //     },
    //   })
    //   .then((res) => {
    //     setData(res.data);
    //     setLoading(false);
    //   })
    //   .catch((error) => {
    //     console.log(error);
    //   });
  }, []);

  useEffect(() => {}, [category, icon, SubmitState]);
  useEffect(() => {
    console.log(data, 'useEffect ');
  }, [data]);
  return (
    <>
      {loading ? (
        <div
          className='loader'
          style={{
            height: '400px',
            width: '100%',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <HashLoader color={'#E1E8EB'} size={50} />
        </div>
      ) : (
        <div className='cat_container'>
          <ToastContainer />

          <div className='cat_form_conts_header'>
            {props.CatHandler.Mode.includes('add') ? (
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'space-evenly',
                  width: '100%',
                }}>
                <h1>Ajouter une Categorie</h1>
              </div>
            ) : (
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'space-evenly',
                  width: '100%',
                }}>
                <h1>Modifier la Categorie </h1>
              </div>
            )}
          </div>

          <form>
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                width: '100%',
              }}>
              <label
                htmlFor='icon-button-file'
                style={{ position: 'relative', left: '180px', top: '166px' }}>
                <Input
                  accept='image/*'
                  onChange={(e) => {
                    setIcon(URL.createObjectURL(e.target.files[0]));

                    setCaticon(e.target.files);
                  }}
                  id='icon-button-file'
                  type='file'
                />
                <IconButton
                  style={{ color: '#ea7c69', backgroundColor: '#fff' }}
                  aria-label='upload picture'
                  component='span'>
                  <PhotoCamera />
                </IconButton>
              </label>

              <div className='categoryimg'>
                {!icon && props.CatHandler.Mode.includes('Edit') ? (
                  <img src={props.CatHandler.cat.icon} alt='img' />
                ) : (
                  <img src={icon} alt='img' />
                )}
              </div>
            </div>
            <div className='row'>
              <div className='input_groups'>
                <label htmlFor='category'>nom </label>
                <input
                  className='input_groups_input'
                  type='category'
                  name='category'
                  id='category'
                  value={category}
                  onChange={(e) => setCategory(e.target.value)}
                />
              </div>
            </div>
          </form>
          <div className='editcategorie'>
            {props.CatHandler.Mode.includes('Edit') ? (
              <button id='delete' onClick={(e) => handleDeleteCategorie(e)}>
                Supprimer
              </button>
            ) : (
              ' '
            )}
            <button onClick={(e) => submitHandler(e)}>{SubmitState}</button>
          </div>
        </div>
      )}
    </>
  );
};

export default Categories;
