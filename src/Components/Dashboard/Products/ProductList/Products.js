/** @format */

import React, { useEffect, useState } from 'react';
import { ToastContainer } from 'react-toastify';
import Grid from '@mui/material/Grid';
import ProductGrid from './Common/ProductGrid/ProductGrid';
import ProductMenu from './Common/ProductMenu/ProductMenu';
import './products.scss';

const Products = () => {
  const [showsidebar, setShowsidebar] = useState(false);
  const [MenuUpdate, setMenuUpdate] = useState(false);
  const [ProdToUpdate, SetProdToUpdate] = useState({ Mode: 'Add' });
  const [CatToUpdate, SetCatToUpdate] = useState({ Mode: 'add' });
  const [Categories, setCategories] = useState([]);
  const [Type, SetType] = useState('product');
  const [updateCat, setUpdateCat] = useState(false);
  useEffect(() => {}, [Categories, Type, CatToUpdate, updateCat]);

  useEffect(() => {
    console.log('showsidebar', showsidebar);
  }, [showsidebar]);

  const CategoriesHandler = (cat) => {
    console.log(cat, 'cats');
    setCategories(cat);
  };

  const HandleMenuUpdate = () => {
    setMenuUpdate(!MenuUpdate);
    if (Type === 'category') {
      SetCatToUpdate({ Mode: 'Add' });
    }
  };

  const HandleAddProduct = () => {
    SetProdToUpdate({ Mode: 'Add' });
    SetType('product');
    if (!showsidebar) {
      setShowsidebar(true);
    }
  };

  const HandleEditCategories = (cat) => {
    console.log('edit', cat);
    SetType('category');

    SetCatToUpdate({ Mode: 'edit', ...cat });

    if (!showsidebar) {
      setShowsidebar(true);
    }
  };

  const HandleEditProduct = (prop) => {
    console.log(prop, 'main');
    SetType('product');

    SetProdToUpdate(prop);
    if (!showsidebar) {
      setShowsidebar(true);
    }
  };

  const HandleAddCategory = () => {
    console.log('add category');
    SetCatToUpdate({ Mode: 'add' });
    SetType('category');
    if (!showsidebar) {
      setShowsidebar(true);
    }
  };

  const handleDeleteCallback = (cat) => {
    setUpdateCat(!updateCat);
  };
  return (
    <div className='dash_products'>
      {/* <Loading loading={loading} /> */}
      <ToastContainer
        position='top-right'
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme='colored'
      />
      {/* <AddProduct /> */}
      {/* {ProductList !== undefined && <ProductList ProdList={ListProduct} />} */}
      <Grid container spacing={3} className='ProductsMain'>
        <Grid item xs={12} sm={12} md={showsidebar ? 8 : 12}>
          <ProductGrid
            sidebar={HandleAddProduct}
            Updated={MenuUpdate}
            updatehandler={HandleMenuUpdate}
            EditHandler={HandleEditProduct}
            CategoriesHandler={CategoriesHandler}
            HandleEditCategories={HandleEditCategories}
            sidebarcat={HandleAddCategory}
            candleCatUpdate={updateCat}
          />
        </Grid>
        {showsidebar && (
          <Grid item xs={12} sm={12} md={4}>
            <ProductMenu
              updatehandler={HandleMenuUpdate}
              MenuHandler={ProdToUpdate}
              Categories={Categories}
              CatHandler={CatToUpdate}
              Type={Type}
              CatDeletehandler={handleDeleteCallback}
              handleCloseMenu={() => setShowsidebar(false)}
            />
          </Grid>
        )}
      </Grid>
    </div>
  );
};

export default Products;
