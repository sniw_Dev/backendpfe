/** @format */

/** @format */

import React, { useEffect, useState } from 'react';
import Grid from '@mui/material/Grid';
import { Link, useNavigate } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faHeart,
  faSearch,
  faShoppingBag,
  faTimes,
} from '@fortawesome/free-solid-svg-icons';
import axios from 'axios';
import ReqLoading from '../../../Loading/ReqLoading';
import ProductAddModal from '../ProductFormAdd/ProductAddModal';
// import EditProduct from '../ProductFormEdit/EditProduct';
import ProductEditModal from '../ProductFormEdit/ProductEditModal';
import './products.scss';
const ProductList = ({ ProdList }) => {
  const navigate = useNavigate();
  const [active, setActive] = useState('');
  const [search, setSearch] = useState('');
  const [realSearch, setRealSearch] = useState(null);
  const [reqLoading, setReqLoading] = useState(false);
  const [ListProd, SetListProd] = useState();

  useEffect(() => {
    console.log(ProdList);
  }, [ListProd]);
  useEffect(() => {
    console.log(ProdList, 'prodlist');

    if (ProdList) {
      SetListProd(ProdList);
    }
  }, [ProdList]);

  useEffect(() => {
    console.log(ListProd, 'list_prod');
  }, [ListProd]);

  let toggleHundel = (e) => {
    active === '' ? setActive('active') : setActive('');
  };

  function realTimeSearchHandler(val) {
    console.log(val);
    setSearch(val);
    setRealSearch(null);
    axios
      .get(`http://127.0.0.1:5000/product/search?search=${val}`)
      .then((res) => {
        if (val === '') {
          return setRealSearch(null);
        }

        setRealSearch(res.data.map((d) => ({ title: d.title, id: d._id })));
        SetListProd(res.data);
      });
  }

  function searchBtnHandler() {
    search !== ''
      ? navigate(`/products?search=${search}`)
      : toast.warning('Enter anything to search😊');
    setSearch('');
    setRealSearch(null);
  }

  function resetSearch() {
    setRealSearch(null);
    setSearch('');
  }

  function HandleDeleteProduct(Id) {
    setReqLoading(true);
    axios
      .delete(`http://127.0.0.1:5000/product/admin/product/delete/${Id}`, {
        headers: {
          Authorization: `Bearer ${localStorage.token}`,
        },
      })
      .then((res) => {
        SetListProd((data) => data.filter((product) => product._id !== Id));
        setReqLoading(false);
        toast.success(`${res.data.title} deleted successfully`, {
          position: 'top-right',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  return (
    <div>
      <Grid container spacing={3}>
        <Grid className='Product_List_Head' item xs={12}>
          <div className={`search_over ${active}`}>
            <button className='toggle-close' onClick={(_) => toggleHundel()}>
              <FontAwesomeIcon icon={faTimes} />
            </button>
            <div className='search-box'>
              <input
                type='text'
                name='search'
                className='search'
                placeholder='Search...'
                value={search}
                onChange={(e) => realTimeSearchHandler(e.target.value)}
              />

              {realSearch ? (
                <div className='search_key_cont'>
                  {realSearch.map((data, i) => (
                    <span>{data.title}</span>
                  ))}
                </div>
              ) : null}

              <button
                className='search-btn'
                aria-label='search-btn'
                onClick={searchBtnHandler}>
                <FontAwesomeIcon icon={faSearch} />
              </button>
            </div>
          </div>

          <div>
            <ProductAddModal />
          </div>
        </Grid>
        <Grid item xs={12}>
          <table className='Product_table'>
            <thead>
              <tr>
                <th>Photo</th>
                <th>title</th>
                <th>category</th>
                <th>company</th>
                <th>price</th>
                <th>options</th>
              </tr>
            </thead>
            <tbody>
              {ListProd !== undefined
                ? ListProd.map((Prod) => {
                    return (
                      <tr key={Prod._id}>
                        <td className='col-1'>
                          <img
                            src={Prod.images[0] ? Prod.images[0] : 'Not Found'}
                            alt='Not found'
                          />
                        </td>
                        <td>{Prod.title}</td>
                        <td>{Prod.category || 'unknown'}</td>
                        <td>{Prod.company || 'unknown'}</td>
                        <td>{Prod.price || '0'} DT</td>

                        <td>
                          <div>
                            <ProductEditModal Product={Prod} />
                          </div>

                          <button
                            className='delete'
                            id={Prod._id}
                            onClick={(e) => {
                              HandleDeleteProduct(Prod._id);
                              console.log(`Delete  Product ${Prod.title}`);
                            }}>
                            Delete
                          </button>
                        </td>
                      </tr>
                    );
                  })
                : ''}
            </tbody>
          </table>
        </Grid>
      </Grid>
    </div>
  );
};

export default ProductList;
