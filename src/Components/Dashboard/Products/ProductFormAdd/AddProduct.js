/** @format */

import React, { useEffect, useState, useRef } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import { useSelector } from 'react-redux';
import HashLoader from 'react-spinners/HashLoader';
import Swal from 'sweetalert2';

import axios from 'axios';

import './products.scss';

const AddProduct = (props) => {
  const user = useSelector((state) => state.user.user);

  useEffect(() => {
    console.log(user, 'user');
  }, [user]);

  useEffect((_) => {}, []);

  const formRef = useRef(null);
  const [data, setData] = useState([]);
  const [reqloading, setReqLoading] = useState(false);
  const [loading, setLoading] = useState(false);
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [company, setCompany] = useState('');
  const [category, setCategory] = useState('');
  const [specifications, setSpecifications] = useState('');
  const [price, setPrice] = useState('');
  const [photos, setPhotos] = useState();

  let errors = [];
  useEffect(() => {
    console.log(photos);
  }, [photos]);

  useEffect(() => {
    if (props.Categories.length > 0) {
      setData(props.Categories);
    }
  }, [props.Categories]);

  useEffect(() => {
    console.log(props.MenuHandler);

    if (props.MenuHandler.Mode === 'Edit') {
      setTitle(props.MenuHandler.product.title);
      setDescription(props.MenuHandler.product.description);
      setCompany(props.MenuHandler.product.company);
      setCategory(props.MenuHandler.product.category);
      setSpecifications(props.MenuHandler.product.specifications);
      setPrice(props.MenuHandler.product.price);
    } else {
      props.Categories.length > 0 && setCategory(props.Categories[0].name);
      setReqLoading(false);
      setTitle('');
      setDescription('');
      setCompany('');
      setSpecifications('');
      setPrice('');
      formRef.current.reset();
    }
  }, [props.MenuHandler]);

  function formValidation(data) {
    errors = [];
    for (let key in data) {
      if (data[key] === '') {
        errors.push(`${key} is empty!`);
      }
    }
    if (!photos && props.MenuHandler.Mode === 'Add') {
      errors.push(`photos is empty!`);
    }
  }

  const addProductHandler = (e) => {
    e.preventDefault();
    let formData = new FormData();
    let productData = {
      ProductOwner: user._id,
      title,
      description,
      company,
      category,
      specifications,
      price,
    };
    formValidation(productData);
    if (errors.length !== 0) {
      errors.forEach((error, i) => {
        toast.error(error);
      });
      return;
    }

    setReqLoading(true);
    for (let key in productData) {
      formData.append(key, productData[key]);
    }
    [...photos].forEach((photo) => {
      formData.append('photos', photo);
    });
    console.log(formData.get('photos'));
    axios
      .post(`http://127.0.0.1:5000/product/add`, formData, {
        headers: {
          Authorization: `Bearer ${localStorage.token}`,
          'Content-Type': 'multipart/form-data',
        },
      })
      .then((res) => {
        props.updatehandler();
        toast.success('product added successfully😁');
        setCategory(data[0].name);
        setReqLoading(false);
        setTitle('');
        setDescription('');
        setCompany('');
        setSpecifications('');
        setPrice('');
        formRef.current.reset();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const EditProductHandler = (e) => {
    e.preventDefault();
    let formData = new FormData();
    let productData = {
      ProductOwner: user._id,
      title,
      description,
      company,
      category,
      specifications,
      price,
    };
    formValidation(productData);
    if (errors.length !== 0) {
      errors.forEach((error, i) => {
        toast.error(error);
      });
      return;
    }

    setReqLoading(true);
    for (let key in productData) {
      formData.append(key, productData[key]);
    }
    if (photos) {
      [...photos].forEach((photo) => {
        formData.append('photos', photo);
      });
      console.log(formData.get('photos'));
    }
    axios
      .post(
        `http://127.0.0.1:5000/api/v1/${user._id}/product/update/${props.MenuHandler.product._id}
`,
        formData,
        {
          headers: {
            Authorization: `Bearer ${localStorage.token}`,
            'Content-Type': 'multipart/form-data',
          },
        }
      )
      .then((res) => {
        props.updatehandler();
        toast.success('product Updated successfully😁');
        setCategory(data[0].name);
        setReqLoading(false);
        setTitle('');
        setDescription('');
        setCompany('');
        setSpecifications('');
        setPrice('');
        formRef.current.reset();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const ResetForm = () => {
    props.Categories.length > 0 && setCategory(props.Categories[0].name);

    setReqLoading(false);
    setTitle('');
    setDescription('');
    setCompany('');
    setSpecifications('');
    setPrice('');
    formRef.current.reset();
  };

  const handleDeleteProduct = () => {
    Swal.fire({
      title: 'Confirmer la suppression',
      text: 'voulez vous supprimer cette article ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Annuler',

      confirmButtonText: 'Oui, supprimer!',
    }).then((result) => {
      if (result.value) {
        console.log('swal', result.value);
        axios
          .post(
            `http://127.0.0.1:5000/api/v1/${user._id}/product/delete/${props.MenuHandler.product._id}`
          )
          .then((res) => {
            console.log(res);
            props.updatehandler();
            toast.success(`l'article est supprimé avec succès 😁`);
            ResetForm();
          })
          .catch((error) => {});
      }
    });
  };

  return (
    <div>
      <ToastContainer
        position='top-right'
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme='colored'
      />
      {reqloading === true ? (
        <div
          style={{
            height: '100vh',
            width: '100%',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <HashLoader color={'#E1E8EB'} size={150} />
        </div>
      ) : (
        <div className='product_form_conts'>
          <div className='product_form_conts_header'>
            {props.MenuHandler.Mode === 'Add' ? (
              <h1>Ajouter un article</h1>
            ) : (
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'space-evenly',
                  width: '100%',
                }}>
                <h1>Modifier l'article</h1>
                <button
                  onClick={(e) => handleDeleteProduct()}
                  className='delete_button'>
                  Supprimer
                </button>
              </div>
            )}
          </div>

          <form ref={formRef}>
            <div className='row'>
              <div className='input_groups'>
                <label htmlFor='title'>title</label>
                <input
                  type='text'
                  name='title'
                  id='title'
                  value={title}
                  onChange={(e) => setTitle(e.target.value)}
                />
              </div>
              <div className='input_groups'>
                <label htmlFor='description'>description</label>
                <input
                  type='text'
                  name='description'
                  id='description'
                  value={description}
                  onChange={(e) => setDescription(e.target.value)}
                />
              </div>
              <div className='input_groups'>
                <label htmlFor='company'>company</label>
                <input
                  type='text'
                  name='company'
                  id='company'
                  value={company}
                  onChange={(e) => setCompany(e.target.value)}
                />
              </div>
              <div className='input_groups'>
                <label htmlFor='category'>category</label>
                <select
                  name='category'
                  value={category}
                  onChange={(e) => setCategory(e.target.value)}>
                  {data.map((categ) => {
                    return (
                      <option value={categ.name} key={categ._id}>
                        {categ.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className='input_groups'>
                <label htmlFor='price'>price</label>
                <input
                  type='number'
                  name='price'
                  id='price'
                  value={price}
                  onChange={(e) => setPrice(e.target.value)}
                />
              </div>
              <div className='input_groups'>
                <label htmlFor='images'>image</label>
                <input
                  type='file'
                  name='image'
                  id='image'
                  onChange={(e) => setPhotos(e.target.files)}
                  multiple
                />
              </div>
              <div className='input_groups'>
                <label htmlFor='specifications'>specifications</label>
                <textarea
                  name='specifications'
                  id='specifications'
                  value={specifications}
                  onChange={(e) =>
                    setSpecifications(e.target.value)
                  }></textarea>
              </div>
            </div>
            {props.MenuHandler.Mode === 'Add' ? (
              <button onClick={(e) => addProductHandler(e)}>Ajouter</button>
            ) : (
              <button onClick={(e) => EditProductHandler(e)}>Modifier</button>
            )}
          </form>
        </div>
      )}
    </div>
  );
};

export default AddProduct;
