/** @format */

import React, { useEffect, useState, useRef } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import axios from 'axios';

import './products.scss';

const EditProduct = ({ Product }) => {
  useEffect(() => {
    console.log('ProductEdit', Product);
  }, [Product]);

  useEffect((_) => {
    setLoading(true);
    axios
      .get(`http://127.0.0.1:5000/category`, {
        headers: {
          Authorization: `Bearer ${localStorage.token}`,
        },
      })
      .then((res) => {
        setData(res.data);
        setCategory(res.data[0].name);
        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  const formRef = useRef(null);
  const [data, setData] = useState([]);
  const [reqloading, setReqLoading] = useState(false);
  const [loading, setLoading] = useState(false);
  const [title, setTitle] = useState(Product.title);
  const [description, setDescription] = useState(Product.description);
  const [company, setCompany] = useState(Product.company);
  const [category, setCategory] = useState(Product.category);
  const [specifications, setSpecifications] = useState(Product.specifications);
  const [price, setPrice] = useState(Product.price);
  const [photos, setPhotos] = useState();

  let errors = [];
  useEffect(() => {
    console.log(photos);
  }, [photos]);
  function formValidation(data) {
    errors = [];
    for (let key in data) {
      if (data[key] === '') {
        errors.push(`${key} is empty!`);
      }
    }
    if (!photos) {
      errors.push(`photos is empty!`);
    }
  }

  const addProductHandler = (e) => {
    e.preventDefault();
    let formData = new FormData();
    let productData = {
      title,
      description,
      company,
      category,
      specifications,
      price,
    };
    formValidation(productData);
    if (errors.length !== 0) {
      errors.forEach((error, i) => {
        toast.error(error);
      });
      return;
    }

    setReqLoading(true);
    for (let key in productData) {
      formData.append(key, productData[key]);
    }
    console.log(photos);
    [...photos].forEach((photo) => {
      formData.append('photos', photo);
    });
    console.log(formData.get('photos'));
    axios
      .post(
        `http://127.0.0.1:5000/product/admin/product/update/${Product._id}`,
        formData,
        {
          headers: {
            Authorization: `Bearer ${localStorage.token}`,
            'Content-Type': 'multipart/form-data',
          },
        }
      )
      .then((res) => {
        toast.success('product updated successfully😁');
        setCategory(data[0].name);
        setReqLoading(false);
        setTitle('');
        setDescription('');
        setCompany('');
        setSpecifications('');
        setPrice('');
        formRef.current.reset();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div>
      <ToastContainer
        position='top-right'
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme='colored'
      />

      <div className='product_form_cont'>
        <form ref={formRef}>
          <div className='row'>
            <div className='input_group'>
              <label htmlFor='title'>title</label>
              <input
                type='text'
                name='title'
                id='title'
                value={title}
                onChange={(e) => setTitle(e.target.value)}
              />
            </div>
            <div className='input_group'>
              <label htmlFor='description'>description</label>
              <input
                type='text'
                name='description'
                id='description'
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              />
            </div>
          </div>
          <div className='row'>
            <div className='input_group'>
              <label htmlFor='company'>company</label>
              <input
                type='text'
                name='company'
                id='company'
                value={company}
                onChange={(e) => setCompany(e.target.value)}
              />
            </div>
            <div className='input_group'>
              <label htmlFor='category'>category</label>
              <select
                name='category'
                value={category}
                onChange={(e) => setCategory(e.target.value)}>
                {data.map((categ) => {
                  return (
                    <option value={categ.name} key={categ._id}>
                      {categ.name}
                    </option>
                  );
                })}
              </select>
            </div>
          </div>
          <div className='row'>
            <div className='input_group'>
              <label htmlFor='price'>price</label>
              <input
                type='number'
                name='price'
                id='price'
                value={price}
                onChange={(e) => setPrice(e.target.value)}
              />
            </div>
            <div className='input_group'>
              <label htmlFor='images'>image</label>
              <input
                type='file'
                name='image'
                id='image'
                onChange={(e) => setPhotos(e.target.files)}
                multiple
              />
            </div>
          </div>
          <div className='row'>
            <div className='input_group'>
              <label htmlFor='specifications'>specifications</label>
              <textarea
                name='specifications'
                id='specifications'
                value={specifications}
                onChange={(e) => setSpecifications(e.target.value)}></textarea>
            </div>
          </div>
          <button onClick={(e) => addProductHandler(e)}>Edit</button>
        </form>
      </div>
    </div>
  );
};

export default EditProduct;
