/** @format */

import React, { useEffect, useState } from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import Slide from '@mui/material/Slide';
import EditProduct from './EditProduct';
import './products.scss';
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />;
});

export default function ProductEditModal({ Product }) {
  useEffect(() => {
    console.log(Product, 'product');
  }, [Product]);

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button variant='outlined' className='add_btn' onClick={handleClickOpen}>
        Edit
      </Button>
      <Dialog
        className='dash_products'
        maxWidth={'lg'}
        fullWidth={true}
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-describedby='alert-dialog-slide-description'>
        <DialogTitle>{'Modifier Produit'}</DialogTitle>
        <div style={{ width: '100%' }}>
          {Product && <EditProduct Product={Product} />}
        </div>
      </Dialog>
    </div>
  );
}
