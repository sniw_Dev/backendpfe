/** @format */

import React, { useEffect, useState } from 'react';
import './dashboard-content.scss';
import axios from 'axios';
import Loading from '../Loading/Loading';
import { io } from 'socket.io-client';
import { ToastContainer, toast } from 'react-toastify';
import Grid from '@mui/material/Grid';
import Header from './Common/Header/Header';
import StatsCards from './Common/StatsCards/StatsCards';
import OrderTable from './Common/OrderTable/OrderTable';
import TopOrders from './Common/TopOrders/TopOrders';
import ChartCard from './Common/ChartCard/ChartCard';
const DashboardContent = () => {
  const socket = io('http://127.0.0.1:5000');

  const [loading, setLoading] = useState(false);
  const [admins, setAdmins] = useState(0);
  const [users, setUsers] = useState(0);
  const [products, setProducts] = useState(0);
  const [orders, setOrders] = useState(0);
  useEffect(() => {
    socket.on('order:new', (args) => {
      console.log('new order');

      getOrders().then((res) => {
        setOrders(res.data.length);
      });
    });
  }, []);

  useEffect(() => {}, [orders]);

  function getAdmins() {
    return axios.get(`http://127.0.0.1:5000/admins`, {
      headers: {
        Authorization: `Bearer ${localStorage.token}`,
      },
    });
  }
  function getUsers() {
    return axios.get(`http://127.0.0.1:5000/users`, {
      headers: {
        Authorization: `Bearer ${localStorage.token}`,
      },
    });
  }

  function getProducts() {
    return axios.get(`http://127.0.0.1:5000/product/count`, {
      headers: {
        Authorization: `Bearer ${localStorage.token}`,
      },
    });
  }
  function getOrders() {
    return axios.get(`http://127.0.0.1:5000/order/all`, {
      headers: {
        Authorization: `Bearer ${localStorage.token}`,
      },
    });
  }

  useEffect(() => {
    setLoading(true);
    axios
      .all([getAdmins(), getUsers(), getProducts(), getOrders()])
      .then(
        axios.spread((...responses) => {
          setAdmins(responses[0].data.length + 1);
          setUsers(responses[1].data.length);
          setProducts(responses[2].data.count);
          setOrders(responses[3].data.length);
          setLoading(false);
        })
      )
      .catch((error) => {
        console.log(error);
      });
  }, []);
  return (
    <div className='dashboard'>
      <ToastContainer />

      <Loading loading={loading} />

      <Grid container spacing={3} className='MainContainer'>
        <Grid item xs={12} sm={12} md={8} className='leftContainer'>
          <Grid container spacing={3}>
            <Grid item xs={12} sm={12} md={12}>
              <Header />
            </Grid>

            <Grid item xs={12} sm={12} md={12}>
              <StatsCards />
            </Grid>

            <Grid item xs={12} sm={12} md={12}>
              <OrderTable />
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} sm={12} md={4} className='rightContainer'>
          <Grid container spacing={3}>
            <Grid item xs={12} sm={12} md={12}>
              <TopOrders />
            </Grid>

            <Grid item xs={12} sm={12} md={12}>
              <ChartCard />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};

export default DashboardContent;
