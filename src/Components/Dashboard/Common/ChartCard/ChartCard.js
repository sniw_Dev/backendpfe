/** @format */

import React, { useState, useEffect } from 'react';
import './ChartCard.scss';
import Chart from 'react-apexcharts';

const ChartCard = () => {
  const [state, Setstate] = useState({
    series: [44, 55, 67, 83],
    options: {
      chart: {
        height: 350,
        type: 'radialBar',
      },
      plotOptions: {
        radialBar: {
          dataLabels: {
            name: {
              fontSize: '22px',
            },
            value: {
              fontSize: '16px',
            },
            total: {
              show: true,
              label: 'Total',
              formatter: function (w) {
                // By default this function returns the average of all series. The below is just an example to show the use of custom formatter function
                return 249;
              },
            },
          },
        },
      },
      labels: ['Apples', 'Oranges', 'Bananas', 'Berries'],
    },
  });
  return (
    <div className='chart_container'>
      <div className='MostOrderschart__header'>
        <span>Most Type of Order</span>
      </div>
      <div id='chart'>
        <Chart
          options={state.options}
          series={state.series}
          type='radialBar'
          height={350}
        />
      </div>
    </div>
  );
};

export default ChartCard;
