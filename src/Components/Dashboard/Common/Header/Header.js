/** @format */

import React from 'react';
import './Header.scss';
const Header = () => {
  return (
    <div className='Header'>
      <div className='Header__title'>
        <span>Dashboard</span>
      </div>
      <div className='Header__subtitle'>
        <span>Tuesday 2 Feb, 2021</span>
      </div>
    </div>
  );
};

export default Header;
