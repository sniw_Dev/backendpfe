/** @format */

import React from 'react';
import Button from '@mui/material/Button';

import './TopOrders.scss';
const TopOrders = () => {
  return (
    <div className='TopOrders__container'>
      <div className='TopOrders__header'>
        <span>Most Ordered</span>
      </div>
      <div className='TopOrders__main'>
        <div className='TopOrders__main-item'>
          <div className='TopOrders__main-item-img'>
            <img
              src='http://pngimg.com/uploads/cocktail/cocktail_PNG121.png'
              alt='product'
            />
          </div>
          <div className='TopOrders__main-item-content'>
            <div className='TopOrders__main-item-header'>
              <span>Cocktail Yummy </span>
            </div>

            <div className='TopOrders__main-item-sub-header'>
              <span>200 dishes ordered</span>
            </div>
          </div>
        </div>
        <div className='TopOrders__main-item'>
          <div className='TopOrders__main-item-img'>
            <img
              src='http://pngimg.com/uploads/cocktail/cocktail_PNG121.png'
              alt='product'
            />
          </div>
          <div className='TopOrders__main-item-content'>
            <div className='TopOrders__main-item-header'>
              <span>Cocktail Yummy </span>
            </div>

            <div className='TopOrders__main-item-sub-header'>
              <span>200 dishes ordered</span>
            </div>
          </div>
        </div>

        <div className='TopOrders__main-item'>
          <div className='TopOrders__main-item-img'>
            <img
              src='http://pngimg.com/uploads/cocktail/cocktail_PNG121.png'
              alt='product'
            />
          </div>
          <div className='TopOrders__main-item-content'>
            <div className='TopOrders__main-item-header'>
              <span>Cocktail Yummy </span>
            </div>

            <div className='TopOrders__main-item-sub-header'>
              <span>200 dishes ordered</span>
            </div>
          </div>
        </div>
      </div>
      <div className='TopOrders__footer'>
        <Button
          variant='outlined'
          style={{
            width: '100%',
            height: '50px',
            color: 'rgb(236, 105, 105)',
            borderColor: 'rgb(236, 105, 105)',
          }}>
          Voir tout
        </Button>
      </div>
    </div>
  );
};

export default TopOrders;
