/** @format */

import React from 'react';
import './StatsCards.scss';
import Grid from '@mui/material/Grid';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import {
  faShippingFast,
  faShoppingBag,
  faUsers,
  faUsersCog,
} from '@fortawesome/free-solid-svg-icons';
const StatsCards = () => {
  return (
    <Grid container spacing={3}>
      <Grid item xs={12} sm={4}>
        <div className='card_stats'>
          <div className='card-header'>
            <span style={{ color: '#FFB572' }}>
              <FontAwesomeIcon icon={faShippingFast} />
            </span>
          </div>
          <div className='card-content'>10,243.00 DT</div>
          <div className='card-footer'>Total Revenue</div>
        </div>
      </Grid>
      <Grid item xs={12} sm={4}>
        <div className='card_stats'>
          <div className='card-header'>
            <span style={{ color: '#FFB572' }}>
              <FontAwesomeIcon icon={faShippingFast} />
            </span>
          </div>
          <div className='card-content'>10,243.00 DT</div>
          <div className='card-footer'>Total Revenue</div>
        </div>
      </Grid>
      <Grid item xs={12} sm={4}>
        <div className='card_stats'>
          <div className='card-header'>
            <span style={{ color: '#FFB572' }}>
              <FontAwesomeIcon icon={faShippingFast} />
            </span>
          </div>
          <div className='card-content'>10,243.00 DT</div>
          <div className='card-footer'>Total Revenue</div>
        </div>
      </Grid>
    </Grid>
  );
};

export default StatsCards;
