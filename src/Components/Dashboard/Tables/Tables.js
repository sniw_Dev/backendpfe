/** @format */

import React, { useEffect, useState } from 'react';
import Grid from '@mui/material/Grid';
import { Link, useNavigate } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faHeart,
  faSearch,
  faShoppingBag,
  faTimes,
} from '@fortawesome/free-solid-svg-icons';
import axios from 'axios';
import ReqLoading from '../../Loading/ReqLoading';
import ProductAddModal from '../Products/ProductFormAdd/ProductAddModal';
import FormsModal from '../../Helpers/FormsModal';
// import EditProduct from '../ProductFormEdit/EditProduct';
import ProductEditModal from '../Products/ProductFormEdit/ProductEditModal';
import '../Products/ProductList/products.scss';
import './Tables.scss';
const Tables = ({ ProdList }) => {
  const QRCode = require('qrcode.react');

  const navigate = useNavigate();
  const [active, setActive] = useState('');
  const [search, setSearch] = useState('');
  const [realSearch, setRealSearch] = useState(null);
  const [reqLoading, setReqLoading] = useState(false);
  const [ListProd, SetListProd] = useState();
  const [ListTables, SetListTables] = useState();

  useEffect(() => {
    if (!ListTables) {
      axios
        .get('http://127.0.0.1:5000/table/all')
        .then((res) => {
          SetListTables(res.data);
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }, [ListTables]);

  useEffect(() => {
    console.log(ProdList);
  }, [ListProd]);
  useEffect(() => {
    console.log(ProdList, 'prodlist');

    if (ProdList) {
      SetListProd(ProdList);
    }
  }, [ProdList]);

  useEffect(() => {
    console.log(ListProd, 'list_prod');
  }, [ListProd]);

  let toggleHundel = (e) => {
    active === '' ? setActive('active') : setActive('');
  };

  function realTimeSearchHandler(val) {
    console.log(val);
    setSearch(val);
    setRealSearch(null);
    axios
      .get(`http://127.0.0.1:5000/table/search?search=${val}`)
      .then((res) => {
        if (val === '') {
          return setRealSearch(null);
        }
        console.log(res.data);
        setRealSearch(res.data.map((d) => ({ Name: d.Name, id: d._id })));
        SetListTables(res.data);
      });
  }

  function searchBtnHandler() {
    search !== ''
      ? navigate(`/products?search=${search}`)
      : toast.warning('Enter anything to search😊');
    setSearch('');
    setRealSearch(null);
  }

  function resetSearch() {
    setRealSearch(null);
    setSearch('');
  }
  function RefreshList() {
    SetListTables();
  }

  function HandleDeleteTable(Id) {
    setReqLoading(true);
    axios
      .delete(`http://127.0.0.1:5000/table/delete/${Id}`, {
        headers: {
          Authorization: `Bearer ${localStorage.token}`,
        },
      })
      .then((res) => {
        SetListTables((data) => data.filter((table) => table._id !== Id));
        setReqLoading(false);
        toast.success(`${res.data.title} deleted successfully`, {
          position: 'top-right',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  return (
    <div className='dash_table'>
      <ToastContainer
        position='top-right'
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme='colored'
      />
      <Grid container spacing={3}>
        <Grid className='Product_List_Head' item xs={12}>
          <div className={`search_over ${active}`}>
            <button className='toggle-close' onClick={(_) => toggleHundel()}>
              <FontAwesomeIcon icon={faTimes} />
            </button>
            <div className='search-box'>
              <input
                type='text'
                name='search'
                className='search'
                placeholder='Search...'
                value={search}
                onChange={(e) => realTimeSearchHandler(e.target.value)}
              />

              {realSearch ? (
                <div className='search_key_cont'>
                  {realSearch.map((data, i) => (
                    <span>{data.title}</span>
                  ))}
                </div>
              ) : null}

              <button
                className='search-btn'
                aria-label='search-btn'
                onClick={searchBtnHandler}>
                <FontAwesomeIcon icon={faSearch} />
              </button>
            </div>
          </div>

          <div>
            <FormsModal
              action={'add'}
              type={'table'}
              RefreshList={RefreshList}
            />
          </div>
        </Grid>
        <Grid item xs={12}>
          <table className='Product_table'>
            <thead>
              <tr>
                <th>QrCode</th>
                <th>Number</th>
                <th>Location</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {ListTables !== undefined
                ? ListTables.map((Table) => {
                    return (
                      <tr key={Table._id}>
                        <td className='col-1'>
                          <QRCode
                            value={`http://127.0.0.1:3000/${Table._id}`}
                          />
                          ,
                        </td>
                        <td>{Table.Name}</td>
                        <td>{Table.Location || 'unknown'}</td>

                        <td>
                          <div>
                            <FormsModal
                              action={'edit'}
                              type={'table'}
                              payload={Table}
                              RefreshList={RefreshList}
                            />
                          </div>

                          <button
                            className='delete'
                            id={Table._id}
                            onClick={(e) => {
                              HandleDeleteTable(Table._id);
                              console.log(`Delete  Table ${Table.Name}`);
                            }}>
                            Delete
                          </button>
                        </td>
                      </tr>
                    );
                  })
                : ''}
            </tbody>
          </table>
        </Grid>
      </Grid>
    </div>
  );
};

export default Tables;
