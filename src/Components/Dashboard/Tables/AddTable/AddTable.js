/** @format */

import React, { useEffect, useState, useRef } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import axios from 'axios';
const AddTable = () => {
  const formRef = useRef(null);
  const [reqloading, setReqLoading] = useState(false);
  const [loading, setLoading] = useState(false);
  const [Numero, SetNumero] = useState('');
  const [Emplacement, SetEmplacement] = useState('');
  let errors = [];

  function formValidation(data) {
    errors = [];
    for (let key in data) {
      if (data[key] === '') {
        errors.push(`${key} is empty!`);
      }
    }
  }

  const addTableHandler = (e) => {
    e.preventDefault();
    let formData = new FormData();
    let TableData = {
      Name: Numero,
      Location: Emplacement,
    };
    formValidation(TableData);
    if (errors.length !== 0) {
      errors.forEach((error, i) => {
        toast.error(error);
      });
      return;
    }

    setReqLoading(true);
    for (let key in TableData) {
      formData.append(key, TableData[key]);
    }
    axios
      .post(`http://127.0.0.1:5000/table/add`, {
        Name: Numero,
        Location: Emplacement,
      })
      .then((res) => {
        toast.success('product added successfully😁');
        SetNumero('');
        SetEmplacement('Inside');
        formRef.current.reset();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div>
      <ToastContainer
        position='top-right'
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme='colored'
      />
      <div className='product_form_cont'>
        <form ref={formRef}>
          <div className='row'>
            <div className='input_group'>
              <label htmlFor='title'>Numéro du table</label>
              <input
                type='number'
                name='Table_number'
                id='Table_number'
                value={Numero}
                onChange={(e) => SetNumero(e.target.value)}
              />
            </div>
            <div className='input_group'>
              <label htmlFor='Location'>Emplacement</label>
              <select
                name='Location'
                value={Emplacement}
                onChange={(e) => SetEmplacement(e.target.value)}>
                <option value='Inside'>Inside</option>
                <option value='Upstaire'>Upstaire</option>
                <option value='Outside'>Outside</option>
              </select>
            </div>
          </div>

          <button onClick={(e) => addTableHandler(e)}>Ajouter</button>
        </form>
      </div>
    </div>
  );
};

export default AddTable;
