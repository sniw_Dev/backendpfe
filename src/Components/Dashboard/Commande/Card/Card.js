/** @format */

import React from 'react';
import './Card.scss';
import { useEffect } from 'react';
const Card = (props) => {
  useEffect(() => {
    console.log(props.article, 'props.article');
  }, []);

  return (
    <div className='Card'>
      <div className='Card-Media'>
        <img className='media' src={props.article.images[0]} alt='Card' />
      </div>
      <div className='Card-Content'>
        <div className='Product-Name'>
          <span>{props.article.title}</span>
        </div>
        <div className='Product-price'>
          {' '}
          <span style={{ color: '#ea7c69' }}>{props.article.price}DT</span>
        </div>
      </div>
    </div>
  );
};

export default Card;
