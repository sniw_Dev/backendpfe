/** @format */

import React, { useEffect, useState, useRef } from 'react';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';

import RepeatIcon from '@mui/icons-material/Repeat';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
// import ProductCard from '../ProductCard/ProductCard';
import axios from 'axios';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { useSelector } from 'react-redux';
import HashLoader from 'react-spinners/HashLoader';
import Card from './Card/Card';
const Commande = () => {
  const theme = createTheme({
    palette: {
      primary: {
        // Purple and green play nicely together.
        main: '#EA7C69',
      },
      secondary: {
        // This is green.A700 as hex.
        main: '#1f1d2b',
      },
    },
  });
  const [value, setValue] = React.useState('one');

  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [CatList, SetCatList] = useState([]);
  const [ActiveCategory, SetActiveCategory] = useState(0);
  const user = useSelector((state) => state.user.user);

  useEffect(() => {
    fetchCategories();
  }, []);

  const fetchCategories = () => {
    setLoading(true);
    axios
      .get(`http://127.0.0.1:5000/category`, {
        headers: {
          Authorization: `Bearer ${localStorage.token}`,
        },
      })
      .then((res) => {
        SetCatList([...res.data]);
        if (ActiveCategory !== 0) {
          res.data.map((item, index) => {
            if (index === ActiveCategory) {
              SetActiveCategory(index);
            }
          });
        } else {
          SetActiveCategory(0);
        }

        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const handleChange = (event, newValue) => {
    console.log(newValue, 'cat_id');

    SetActiveCategory(newValue);
  };

  useEffect(() => {
    console.log(ActiveCategory, 'ActiveCategory');
    if (ActiveCategory === CatList.length + 1) {
      fetchCategoriesHandler();
    } else {
      if (CatList.length > 0) {
        fetchdata();
      }
    }
  }, [ActiveCategory]);

  const fetchdata = () => {
    console.log(CatList);
    try {
      axios
        .get(
          `http://localhost:5000/api/v1/${user._id}/product/category/${CatList[ActiveCategory].name}`
        )
        .then((res) => {
          setProducts(res.data);
          setLoading(false);
        });
    } catch (error) {
      fetchCategoriesHandler();
    }
  };

  const fetchCategoriesHandler = () => {
    console.log(CatList);
    axios
      .get(`http://localhost:5000/api/v1/${user._id}/product/category/autres`)
      .then((res) => {
        setProducts(res.data);
        setLoading(false);
      });
  };

  return (
    <div className='ProdGrid__Container'>
      <div className='ProdGrid__header_container'>
        <div className='ProdGrid__header'>
          <span>Products Management</span>
        </div>
        <div className='ProdGrid__header__2'>
          <Button
            variant='outlined'
            className='CategoriesButton'
            startIcon={<RepeatIcon />}>
            Manage Categories
          </Button>{' '}
        </div>
      </div>
      <div className='ProdGrid__body'>
        <Tabs
          style={{}}
          value={ActiveCategory}
          onChange={handleChange}
          textColor='primary'
          indicatorColor='primary'
          aria-label='secondary tabs example'>
          {CatList.length !== 0 &&
            CatList.map((cat, index) => (
              <Tab
                style={{ color: '#EA7C69' }}
                value={index}
                label={cat.name}
                onChange={handleChange}
              />
            ))}
          <Tab
            style={{ color: '#EA7C69' }}
            value={CatList.length + 1}
            label={'autres'}
            onChange={handleChange}
          />
        </Tabs>
        <div>
          <Grid container spacing={3} style={{ marginTop: '20px' }}>
            {products.map((product) => (
              <Grid item xs={12} sm={2}>
                <Card article={product} />
              </Grid>
            ))}
          </Grid>
        </div>
      </div>
    </div>
  );
};

export default Commande;
