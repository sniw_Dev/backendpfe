/** @format */

import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';
import { userSliceActions } from '../../Store/UserSlice';
import ProfileImg from '../../images/person.webp';
import './dash-aside.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faShoppingBag,
  faBars,
  faTachometerAlt,
  faShippingFast,
  faSignOutAlt,
  faCashRegister,
  faChair,
  faStore,
} from '@fortawesome/free-solid-svg-icons';
import table from '../../images/table.png';
import { useDispatch } from 'react-redux';
import ReqLoading from '../Loading/ReqLoading';
import { useState } from 'react';

const ProfieAside = (props) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [reqLoading, setReqLoading] = useState(false);
  const logoutHandler = (e) => {
    e.preventDefault();
    setReqLoading(true);
    axios
      .post(
        `http://127.0.0.1:5000/user/logout`,
        {},
        {
          headers: {
            Authorization: `Bearer ${localStorage.token}`,
          },
        }
      )
      .then((response) => {
        localStorage.clear();
        dispatch(userSliceActions.logout());
        setReqLoading(false);
        navigate('/login');
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div className={`profile_aside`}>
      <ReqLoading loading={reqLoading} />
      <div className='aside_head'></div>
      <div className='aside_navs'>
        <div className='navs'>
          <Link to='/dashboard'>
            <span>
              <FontAwesomeIcon icon={faStore} />
            </span>
          </Link>
        </div>
        <div className='navs'>
          <Link to='/dashboard/commande'>
            <span>
              <FontAwesomeIcon icon={faCashRegister} />
            </span>
          </Link>
        </div>

        <div className='navs'>
          <Link to='/dashboard'>
            <span>
              <FontAwesomeIcon icon={faTachometerAlt} />
            </span>
          </Link>
        </div>
        <div className='navs'>
          <Link to='/dashboard/tables'>
            <span>
              <FontAwesomeIcon icon={faChair} />
            </span>
          </Link>
        </div>

        <div className='navs'>
          <Link to='/dashboard/products'>
            <span>
              <FontAwesomeIcon icon={faShoppingBag} />
            </span>
          </Link>
        </div>
        <div className='navs'>
          <Link to='/dashboard/orders'>
            <span>
              <FontAwesomeIcon icon={faShippingFast} />
            </span>
          </Link>
        </div>
        <div className='navs'>
          <Link to='/dashboard/orders' onClick={(e) => logoutHandler(e)}>
            <span>
              <FontAwesomeIcon icon={faSignOutAlt} />
            </span>
          </Link>
        </div>
      </div>
      <button className='toggle_menu_btn'>
        <FontAwesomeIcon icon={faBars} />
      </button>
    </div>
  );
};

export default ProfieAside;
