/** @format */

import React from 'react';
import './helpers.scss';
const CategoryCard = (props) => {
  return (
    <div className='card'>
      <div className='card-info' style={{}}>
        <img src={props.icon} alt='icon' />
        <p>{props.name}</p>
      </div>
    </div>
  );
};

export default CategoryCard;
