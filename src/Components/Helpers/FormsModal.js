/** @format */

import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import Slide from '@mui/material/Slide';
// import AddProduct from './AddProduct';
// import './products.scss';
import EditTable from '../Dashboard/Tables/EditTable/EditTable';
import AddTable from '../Dashboard/Tables/AddTable/AddTable';
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />;
});

export default function FormsModal(props) {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    props.RefreshList();
  };

  return (
    <div>
      {props.action === 'add' ? (
        <>
          <Button
            variant='outlined'
            className='add_btn'
            onClick={handleClickOpen}>
            add {props.type}
          </Button>
          <Dialog
            className='dash_products'
            maxWidth={'lg'}
            fullWidth={true}
            open={open}
            TransitionComponent={Transition}
            keepMounted
            onClose={handleClose}
            aria-describedby='alert-dialog-slide-description'>
            <DialogTitle>{`Ajouter ${props.type}`}</DialogTitle>

            {props.type === 'table' ? <AddTable /> : null}
            <div style={{ width: '100%' }}></div>
          </Dialog>
        </>
      ) : (
        <>
          <Button
            variant='outlined'
            className='add_btn'
            onClick={handleClickOpen}>
            Edit {props.type}
          </Button>
          <Dialog
            className='dash_products'
            maxWidth={'lg'}
            fullWidth={true}
            open={open}
            TransitionComponent={Transition}
            keepMounted
            onClose={handleClose}
            aria-describedby='alert-dialog-slide-description'>
            <DialogTitle>{`Ajouter ${props.type}`}</DialogTitle>

            {props.type === 'table' ? (
              <EditTable table={props.payload} />
            ) : null}
            <div style={{ width: '100%' }}></div>
          </Dialog>
        </>
      )}
    </div>
  );
}
