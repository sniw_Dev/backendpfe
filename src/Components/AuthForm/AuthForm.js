/** @format */

import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';
import './authform.scss';
import OAuthButton from '../Helpers/OAuthButton';
import FormValid from './FormValid';
import ReqLoading from '../Loading/ReqLoading';
import { ToastContainer, toast } from 'react-toastify';

const AuthForm = (props) => {
  const navigate = useNavigate();
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [raison_sociale, setRaison_sociale] = useState('');
  const [adress, setAdress] = useState('');
  const [phone, setPhone] = useState('');

  const [reqloading, setReqLoading] = useState(false);
  let errors = [];
  function formValidation(data) {
    let reg =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let passReg = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/gm;
    let mobileReg = /^[0-9]{8}$/;
    errors = [];
    for (let key in data) {
      if (data[key] === '') {
        errors.push(`${key} is empty!😊`);
      }
      if (key === 'email' && data[key] !== '' && !reg.test(data[key])) {
        errors.push(`${key} is not valid!😊`);
      }
      if (key === 'password' && data[key] !== '' && !passReg.test(data[key])) {
        errors.push(
          `${key} must contains numbers, lowercase and uppercase letters😊`
        );
      }
      if (key === 'phone' && data[key] !== '' && !mobileReg.test(data[key])) {
        errors.push(`${key} must contains numbers of 8 digit 😊`);
      }
    }
  }

  function submitHandler(e) {
    e.preventDefault();
    let data = {
      name,
      email,
      password,
      raison_sociale,
      adress,
      phone,
    };
    formValidation(data);
    if (errors.length !== 0) {
      errors.forEach((error, i) => {
        toast.error(error, {
          position: 'top-right',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      });
      return;
    }
    setReqLoading(true);
    axios
      .post(`http://127.0.0.1:5000/user/register`, data)
      .then((response) => {
        setReqLoading(false);
        navigate('/login');
      })
      .catch((error) => {
        setReqLoading(false);
        console.clear();
        toast.error('email is used, please try another one😊', {
          position: 'top-right',
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      });
  }

  return (
    <div className='form'>
      <ReqLoading loading={reqloading} />
      <form className='form_signup'>
        <h2 className='form_title'>inscription</h2>
        {/* <OAuthButton /> */}
        {/* <div className='or_line'>OR</div> */}
        <div className='form_wrapper'>
          <div className='form_group_left'>
            <span>Nom</span>
            <input
              type='text'
              name='name'
              placeholder=' name '
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
            <span>Email</span>

            <input
              type='email'
              name='email'
              placeholder=' email '
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
            <span>Mot de Passe</span>

            <input
              type='password'
              name='password'
              placeholder=' password '
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <div className='form_group_right'>
            <span>Addresse</span>

            <input
              type='text'
              name='addresse'
              placeholder='addresse'
              value={adress}
              onChange={(e) => setAdress(e.target.value)}
            />
            <span>Raison sociale</span>

            <input
              type='text'
              name='raison_sociale'
              placeholder='raison sociale'
              value={raison_sociale}
              onChange={(e) => setRaison_sociale(e.target.value)}
            />
            <span>Mobile </span>

            <input
              type='number'
              name='mobile'
              placeholder='mobile'
              value={phone}
              onChange={(e) => setPhone(e.target.value)}
            />
          </div>
        </div>
        <button className='submit' onClick={(e) => submitHandler(e)}>
          signup
        </button>
        <p className='redir_signup'>
          have an account ? <Link to='/login'>login</Link>
        </p>
      </form>
      <ToastContainer
        position='top-right'
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme='colored'
      />
    </div>
  );
};

export default AuthForm;
